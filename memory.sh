#!/usr/bin/env bash

HEIGHT=15
WIDTH=60
CHOICE_HEIGHT=4
BACKTITLE="InstallHelper.de"
TITLE="Arbeitsspeicher"
MENU="Wie viel Arbeitsspeicher willst du deinen Minecraft-Server zuweisen?"

OPTIONS=(1 "512MB - 0,5GB"
         2 "1024MB - 1,0GB"
         3 "2048MB - 2,0GB"
         4 "3072MB - 3,0GB"
         5 "4096MB - 4.0GB"
         6 "5120MB - 5,0GB"
         7 "6144MB - 6.0GB"
         8 "7168MB - 7.0GB"
         9 "8192MB - 8.0GB"
         10 "9216MB - 9.0GB"
         11 "10240MB - 10.0GB"
         12 "11264MB - 11.0GB"
         13 "12288MB - 12.0GB"
         14 "13312MB - 13.0GB"
         15 "14336MB - 14.0GB"
         16 "15360MB - 15.0GB"
         17 "16384MB - 16.0GB")

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
        1)
            echo "screen -AmdS minecraft java -Xms512M -Xmx512M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        2)
            echo "screen -AmdS minecraft java -Xms1024M -Xmx1024M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        3)
            echo "screen -AmdS minecraft java -Xms2048M -Xmx2048M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        4)
            echo "screen -AmdS minecraft java -Xms3072M -Xmx3072M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        5)
            echo "screen -AmdS minecraft java -Xms4096M -Xmx4096M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        6)
            echo "screen -AmdS minecraft java -Xms5120M -Xmx5120M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        7)
            echo "screen -AmdS minecraft java -Xms6144M -Xmx6144M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        8)
            echo "screen -AmdS minecraft java -Xms7168M -Xmx7168M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        9)
            echo "screen -AmdS minecraft java -Xms8192M -Xmx8192M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        10)
            echo "screen -AmdS minecraft java -Xms9216M -Xmx9216M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        11)
            echo "screen -AmdS minecraft java -Xms10240M -Xmx10240M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        12)
            echo "screen -AmdS minecraft java -Xms11264M -Xmx11264M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        13)
            echo "screen -AmdS minecraft java -Xms12288M -Xmx12288M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        14)
            echo "screen -AmdS minecraft java -Xms13312M -Xmx13312M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        15)
            echo "screen -AmdS minecraft java -Xms14336M -Xmx14336M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        16)
            echo "screen -AmdS minecraft java -Xms15360M -Xmx15360M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
        17)
            echo "screen -AmdS minecraft java -Xms16384M -Xmx16384M -jar /home/minecraft/paperspigot.jar" > /home/minecraft/start.sh
            ;;
esac