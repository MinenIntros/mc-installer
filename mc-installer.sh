#!/usr/bin/env bash
echo "InstallHelper.de"
echo ""
echo "Little Disclaimer"
echo "English: The Developer is not liable for consequential damage or otherwise."
echo "Deutsch: Der Entwickler haftet nicht für Folgeschäden oder sonstiges."
sleep 5
clear
echo "Minecraft PaperSpigot-Server-Installer"
sleep 1
echo ""
echo "Hinweis: Dieser Script wurde nur für Debian / Ubuntu geschrieben! Keine Gewährleistung bei eventuellen Folgeschäden!"
echo "Ich stehe in keiner jeglichen Art und Weise in Zusammenarbeit mit Mojang oder PaperSpigot. (Mojang, Mojang Studios, Mojang Synergies AB, Microsoft, Microsoft Corporation)"
echo "Dies ist ein nicht-kommerzieller Script."
sleep 7
clear
echo "Script startet automatisch in 10"
sleep 1
clear
echo "Script startet automatisch in 9"
sleep 1
clear
echo "Script startet automatisch in 8"
sleep 1
clear
echo "Script startet automatisch in 7"
sleep 1
clear
echo "Script startet automatisch in 6"
sleep 1
clear
echo "Script startet automatisch in 5"
sleep 1
clear
echo "Script startet automatisch in 4"
sleep 1
clear
echo "Script startet automatisch in 3"
sleep 1
clear
echo "Script startet automatisch in 2"
sleep 1
clear
echo "Script startet automatisch in 1"
sleep 1
clear
echo "Script startet nun. Bitte unterbreche den Script während der Installation nicht."
sleep 5
echo ""
echo "--- Server wird geupdated und geupgraded ---"
echo ""
sleep 3
apt update
apt upgrade -y
echo ""
echo "--- Vorgang abgeschlossen ---"
echo ""
sleep 2
clear
echo ""
echo "--- Installiere Java (OpenJDk 8 - JRE), screen, software-properties-common, gnupg2 und dialog ---"
sleep 3
sudo apt-get install gnupg2 -y
sudo apt-get install software-properties-common -y
wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | sudo apt-key add -
sudo add-apt-repository --yes https://adoptopenjdk.jfrog.io/adoptopenjdk/deb/ -y
sudo apt-get update
sudo apt upgrade -y
sudo apt-get install adoptopenjdk-8-hotspot -y
apt install screen -y
apt install dialog -y
echo ""
echo "--- Vorgang abgeschlossen ---"
echo ""
sleep 3
clear
echo ""
echo "--- Downloade und erstelle erforderliche Dateien ---"
echo ""
sleep 3
mkdir /home/minecraft
cd /home/minecraft/
cd /tmp/
wget --no-check-certificate https://gitlab.com/MinenIntros/mc-installer/-/raw/master/version.sh
chmod +x version.sh
bash version.sh
sleep 1
wget --no-check-certificate https://gitlab.com/MinenIntros/mc-installer/-/raw/master/memory.sh
sed -i -e 's/\r$//' memory.sh
bash memory.sh
rm -r /tmp/memory.sh
wget --no-check-certificate https://gitlab.com/MinenIntros/mc-installer/-/raw/master/mc-eula.sh
chmod +x mc-eula.sh
sed -i -e 's/\r$//' mc-eula.sh
chmod +x /home/minecraft/start.sh
sed -i -e 's/\r$//' /home/minecraft/start.sh
bash mc-eula.sh
rm -r /tmp/mc-eula.sh
sleep 1
echo "--- Vorgang abgeschlossen ---"
sleep 1
cd /tmp/
wget --no-check-certificate https://gitlab.com/MinenIntros/mc-installer/-/raw/master/start-question.sh
sed -i -e 's/\r$//' start-question.sh
chmod +x start-question.sh
./start-question.sh
rm -r start-question.sh
exit 0