#!/usr/bin/env bash

HEIGHT=15
WIDTH=60
CHOICE_HEIGHT=4
BACKTITLE="InstallHelper.de"
TITLE="Version auswahl"
MENU="Welche Server Version willst du installieren?"

OPTIONS=(1 "1.14.4 - PaperSpigot"
         2 "1.13.2 - PaperSpigot"
         3 "1.12.2 - PaperSpigot"
         4 "1.11.2 - PaperSpigot"
         5 "1.10.2 - PaperSpigot"
         6 "1.9.4 - PaperSpigot"
         7 "1.8.8 - PaperSpigot")

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
        1)
            cd /tmp/
            rm -r download
            wget https://papermc.io/api/v1/paper/1.14.4/243/download
            mv download /home/minecraft/paperspigot.jar
            ;;
        2)
            cd /tmp/
            rm -r download
            wget https://papermc.io/api/v1/paper/1.13.2/655/download
            mv download /home/minecraft/paperspigot.jar
            ;;
        3)
            cd /tmp/
            rm -r download
            wget https://papermc.io/api/v1/paper/1.12.2/1618/download
            mv download /home/minecraft/paperspigot.jar
            ;;
        4)
            cd /tmp/
            rm -r download
            wget https://papermc.io/api/v1/paper/1.11.2/1104/download
            mv download /home/minecraft/paperspigot.jar
            ;;
        5)
            cd /tmp/
            rm -r download
            wget https://papermc.io/api/v1/paper/1.10.2/916/download
            mv download /home/minecraft/paperspigot.jar
            ;;
        6)
            cd /tmp/
            rm -r download
            wget https://papermc.io/api/v1/paper/1.9.4/773/download
            mv download /home/minecraft/paperspigot.jar
            ;;
        7)
            cd /tmp/
            rm -r download
            wget https://papermc.io/api/v1/paper/1.8.8/443/download
            mv download /home/minecraft/paperspigot.jar
            ;;
esac