#!/usr/bin/env bash

HEIGHT=15
WIDTH=60
CHOICE_HEIGHT=4
BACKTITLE="InstallHelper.de"
TITLE="Minecraft Server"
MENU="Soll der Minecraft Server gleich auch noch hochgefahren werden?"

OPTIONS=(1 "Ja, bitte den Minecraft Server starten."
         2 "Nein, erledige ich selber.")

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
        1)
            cd /home/minecraft/
            bash start.sh
            echo ""
            echo "Dein Minecraft Server wurde nun gestartet!"
            echo "Vielen Dank für das nutzen des Scripts."
            echo Deine IP Adresse um deinen Minecraft Server zu betreten:
            hostname -I
            echo "Minecraft Port: 25565"
            exit 0
            ;;
        2)
            echo "Wie gewünscht wurde dein Minecraft Server nicht gestartet."
            echo "Du findest den Minecraft-Server-Ordner unter -> '/home/minecraft/'"
            exit 0
            ;;
esac