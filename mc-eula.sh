#!/usr/bin/env bash

HEIGHT=15
WIDTH=60
CHOICE_HEIGHT=4
BACKTITLE="InstallHelper.de"
TITLE="Minecraft EULA"
MENU="Stimmst du als Endnutzer die EULA von Minecraft / Mojang zu? (https://account.mojang.com/terms#commercial)"

OPTIONS=(1 "Ja, ich stimme die EULA von Minecraft (Mojang) zu."
         2 "Nein, ich stimme die EULA von Minecraft (Mojang) nicht zu.")

CHOICE=$(dialog --clear \
                --backtitle "$BACKTITLE" \
                --title "$TITLE" \
                --menu "$MENU" \
                $HEIGHT $WIDTH $CHOICE_HEIGHT \
                "${OPTIONS[@]}" \
                2>&1 >/dev/tty)

clear
case $CHOICE in
        1)
            echo "eula = true" > eula.txt
            mv eula.txt /home/minecraft/eula.txt
            ;;
        2)
            rm -r /home/minecraft/
            exit 0
            ;;
esac